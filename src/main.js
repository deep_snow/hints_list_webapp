import Vue from 'vue'
import App from './App.vue'
import VModal from 'vue-js-modal'
import { store } from './store';
import { router } from './_helpers/router'

Vue.config.productionTip = false

Vue.use(VModal, { dynamic: true, injectModalsContainer: true })

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
