import { apiUrl } from '../config';
import { handleResponse } from '../_helpers/handlers'
import { authHeader } from '../_helpers/auth-header';

function getAll(apiPath) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };

  return fetch(`${apiUrl}/api/${apiPath}`, requestOptions).then(handleResponse);
}

function add(apiPath, newItem) {
  const requestOptions = {
    method: 'POST',
    headers: {
      ...authHeader(),
       "Content-Type": "application/json"
      },
    body: JSON.stringify(newItem)
  };

  return fetch(`${apiUrl}/api/${apiPath}/`, requestOptions)
    .then(handleResponse);
}

function del(apiPath, id) {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader()
  };
  return fetch(`${apiUrl}/api/${apiPath}/${id}/`, requestOptions)
}

function search(apiPath) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${apiUrl}/api/${apiPath}`, requestOptions).then(handleResponse);
}

export const apiService = {
  getAll,
  add,
  del,
  search
}
