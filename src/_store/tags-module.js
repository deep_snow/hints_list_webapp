import { apiService } from '../_api/api-methods';

export const tags = {
  namespaced: true,
  state: {
    status: {},
    tags: []
  },
  actions: {
    getAllTags({ commit }) {
      commit('getAllTagsRequest');

      apiService.getAll('tags/')
        .then(
          tags => commit('getAllTagsSuccess', tags),
          error => commit('getAllTagsFailure', error)
        );
    },
    addTag({ commit }, newTag) {
      commit('addNewTagRequest')

      apiService.add('tags', newTag)
        .then(
          tag => commit('addNewTagSuccess', tag),
          error => commit('addNewTagFailure', error)
        )
    }
  },
  mutations: {
    getAllTagsRequest(state) {
      state.status = { loading: true };
    },
    getAllTagsSuccess(state, tags) {
      state.status = { loading: false };
      state.tags = tags;
    },
    getAllTagsFailure(state, error) {
      state.status = { error };
    },
    addNewTagRequest(state) {
      state.status = { loading: true };
    },
    addNewTagSuccess(state, newTag) {
      state.status = { loading: false };
      state.tags.unshift(newTag);
    },
    addNewTagFailure(state, error) {
      state.status = { error };
    },
  }
}
