import { apiService } from '../_api/api-methods';

export const hints = {
  namespaced: true,
  state: {
    status: {},
    hints: [],
    isSearch: false,
    hasSearchComplited: false,
    queryUrl: '?page=1'
  },
  actions: {
    getAll({ commit }) {
      commit('getAllRequest');
      if (hints.state.queryUrl) {
        apiService.getAll(`commands/${hints.state.queryUrl}`)
          .then(
            hints => commit('getAllSuccess', hints),
            error => commit('getAllFailure', error)
          );
      }
    },
    add({ commit }, newHint) {
      commit('addNewHintRequest')

      apiService.add('commands', newHint)
        .then(
          hint => commit('addNewHintSuccess', hint),
          error => commit('addNewHintFailure', error)
        )
    },
    del({ commit }, id) {
      commit('delHintRequest')

      apiService.del('commands', id)
        .then(
          () => commit('delHintSuccess', id),
          error => commit('delHintFailure', error)
        )
    },
    search({ commit }, query) {
      commit('searchRequest', query);
      if (!hints.state.queryUrl) return;
      if (hints.state.hasSearchComplited) return;

      apiService.search(`commands/${hints.state.queryUrl}`)
        .then(
          hints => commit('searchSuccess', hints),
          error => commit('searchFailure', error)
        );
    },
  },
  mutations: {
    getAllRequest(state) {
      state.status = { loading: true };
      state.queryUrl = state.isSearch ? '?page=1' : state.queryUrl;
      state.isSearch = false;
    },
    getAllSuccess(state, hints) {
      state.status = { loading: false };
      state.hints = hints.previous ? state.hints.concat(hints.results) : hints.results;
      state.queryUrl = hints.next ? hints.next.split('/').pop() : null;
    },
    getAllFailure(state, error) {
      state.status = { error };
    },
    addNewHintRequest(state) {
      state.status = { loading: true };
    },
    addNewHintSuccess(state, newHint) {
      state.status = { loading: false };
      state.hints.unshift(newHint);
    },
    addNewHintFailure(state, error) {
      state.status = { error };
    },
    delHintRequest(state) {
      state.status = { loading: true };
    },
    delHintSuccess(state, id) {
      state.status = { loading: false };
      state.hints = state.hints.filter(el => (el.id != id));
    },
    delHintFailure(state, error) {
      state.status = { error };
    },
    searchRequest(state, query) {
      state.status = { loading: true };
      state.queryUrl = query ? `?page=1&search=${query}` : state.queryUrl;
      state.hasSearchComplited = Boolean(!query && state.hasSearchComplited);
      state.isSearch = true;
    },
    searchSuccess(state, hints) {
      state.status = { loading: false };
      state.hints = hints.previous ? state.hints.concat(hints.results) : hints.results;
      state.queryUrl = hints.next ? hints.next.split('/').pop() : null;
      state.hasSearchComplited = Boolean(!hints.next);
    },
    searchFailure(state, error) {
      state.status = { error };
    },
  }
}
