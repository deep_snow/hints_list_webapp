import Vue from 'vue';
import Vuex from 'vuex';

import { alert } from './_store/alert-module';
import { authentication } from './_store/authentication-module';
import { hints } from './_store/hints-module';
import { tags } from './_store/tags-module';

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    alert,
    authentication,
    hints,
    tags
  }
});
