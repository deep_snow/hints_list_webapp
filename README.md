# hints_list_webapp

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Server

cd /media/cuker/Doc/project/hints_list_server/hints_list_server

source .pyvenv/bin/activate

python3 manage.py runserver

http://127.0.0.1:8000/api/commands/
